## RV32

 1000000000101000001000100101101: 31bit
01000000000101000001000100101101: 32bit

01: 32bit

0000

zyxwvutsrqponmlkgihgfedcba
     * *     *   *  * ** *
00000101000001000100101101
     * *     *   *  * ** *
ZYXWVUTSRQPONMLKGIHGFEDCBA


## RV64

1000000000000000000000000000000000000000000101000001000100101101: 64bit
10: 64bit
000000000000000000000000000000000000
zyxwvutsrqponmlkgihgfedcba
     * *     *   *  * ** *
00000101000001000100101101
     * *     *   *  * ** *
ZYXWVUTSRQPONMLKGIHGFEDCBA

10: 64bit

Bit Character Description
 0*A Atomic extension
 1 B Tentatively reserved for Bit-Manipulation extension
 2*C Compressed extension
 3*D Double-precision floating-point extension
 4 E RV32E base ISA
 5 F Single-precision floating-point extension
 6 G Reserved
 7 H Hypervisor extension
 8*I RV32I/64I/128I base ISA
 9 J Tentatively reserved for Dynamically Translated Languages extension
10 K Reserved
11 L Reserved
12*M Integer Multiply/Divide extension
13 N Tentatively reserved for User-Level Interrupts extension
14 O Reserved
15 P Tentatively reserved for Packed-SIMD extension
16 Q Quad-precision floating-point extension
17 R Reserved
18*S Supervisor mode implemented
19 T Reserved
20*U User mode implemented
21 V Tentatively reserved for Vector extension
22 W Reserved
23 X Non-standard extensions present
24 Y Reserved
25 Z Reserved
