	.text			# Define beginning of text section
	.global	_start		# Define entry _start

_start:
	csrr t0, misa

stop:
	j stop			# Infinite loop to stop execution

	.end			# End of file
