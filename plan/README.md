
# Tasks

本文用于汇总 RISC-V 的相关任务和图文视频等分析成果。

大家可在相应任务后追加 `@your-gitee-id` 来认领任务，也可以提交 PR 新增或修订任务。

相关图文或直播回放成果在发布后将于第一时间更新进来。

## QuickStart

* RISC-V Linux Analyse Howto
    * [如何分析 Linux 内核 RISC-V 架构相关代码][027] @falcon

* RISC-V Linux Distributions
    * [两分钟内极速体验 RISC-V Linux 系统发行版][025] @falcon
    * [5 秒内跨架构运行 RISC-V Ubuntu 22.04 + xfce4 桌面系统][038] @falcon

* Pull Request Howto
    * [直播回放：提交 PR 须知 —— 中英文排版等][044] @falcon
    * [直播回放：RISC-V Linux 技术调研暑期实习启动会暨入职培训][040] @falcon
    * [直播回放：RISC-V Linux 技术调研暑期实习宣讲会][058] @falcon

* Linux Upstream Howto
    * [直播回放：开放实习与兼职岗位并启动 Linux Upstream 计划][053] @falcon

## Experiments

* RISC-V Assembly
    * [视频演示：快速上手 RISC-V 汇编语言实验][046] @tinylab

* RISC-V OS Basics
    * [视频演示：无门槛开展 RVOS 课程实验][047] @tinylab

* RISC-V Linux Kernel Development
    * [视频演示：极速开展 RISC-V Linux v5.17 内核实验][045] @tinylab

## Hardware

* RISC-V CPU Design
    * [大学生 RISC-V 处理器设计实践][041]
    * 从零开始设计 RISC-V CPU @Fajie.WangNiXi

* RISC-V Board Design
    * [开源之夏 2022 - RISC-V 硬件产品开发实录][002]
    * [直播回放：I2C & SPI 用法][060] @taotieren
    * [直播回放：原理图简介 & SDIO 用法][061] @taotieren

* RISC-V Board Usage
    * D1 Board
        * [D1-H 开发板入门][016] @iosdevlog
        * [为哪吒 D1 开发板安装 ArchLinux RISC-V rootfs][015] @tieren
    * Longan Nano
        * [在 Linux 下制作 rv-link 调试器][039] @tieren

## Specs

* ISA
    * [RISC-V ISA 简介][019], [幻灯][004], [直播回放][043] @iOSDevLog
    * [RISC-V 特权指令][033] @pingbo

* psABI
* SBI
* Extensions

## Emulation & Simulation

* rv8
* Spike
* QEMU
* [JuiceVm][008]
* Other
    * [Writing a simple RISC-V emulator in plain C (Base integer, multiplication and csr instructions)][001] @yjmstr

## Basic support

* Build Infrastructure
* User-facing API
* Generic library routines and assembly @郭天佑 TanyoKwok
    * FPU
    * non FPU
    * [non M-extension][012]
    * futex

## Bootloader

* BBL (Berkeley Boot Loader)
* OpenSBI
    * [OpenSBI 快速上手][031] @falcon
* RustSBI
    * RISC-V 引导程序环境：应用&规范，[幻灯][005]，[直播回放][049] @luojia65
* U-Boot
* UEFI
    * [RISC-V UEFI 架构支持详解，第 1 部分 - OpenSBI/U-Boot/UEFI 简介][037] @Jacob

## Boot, Init & Shutdown

* Booting
    * RISC-V Linux 内核启动流程分析，[直播回放][051] @通天塔
    * [RISC-V Linux 启动流程分析][029] @通天塔

* DTS @iOSDevLog
    * [直播回放：RISC-V Linux 设备树语法与格式详解][062] @iOSDevLog

* UEFI support @jiangbo.jacob
* Compressed images
* Early Boot
* earlycon @iosdevlog
* Soc Init
* setup_arch @marycarry
* poweroff
* printk
    * 内核调试基础设施之 printk 初探，[直播回放][054] @iOSDevLog

## Memory Management

* Paging and MMU
    * [RISC-V MMU 概述][030], [直播回放][042] @pwl999

* memblock @tjytimi
* setup_vm @davidlamb
* fixed mapping @tjytimi
* ioremap @tjytimi
* buddy @tjytimi
* per_cpu_cache @tjytimi
* vmalloc @tjytimi
* process's address space @tjytimi
* mmap @tjytimi
* page_fault @tjytimi
* Barriers @cursor_risc-v
* Sparsemem @Jack Y.
* CMA
* DMA
* Hugetlbfs
* memtest
* NUMA
* nommu
* msharefs

## Device, IRQs, Traps and the SBI

* SBI
* Device
* IRQs @通天塔 @williamsun0122
* Traps
* swiotlb

## Timers

* Timers
    * [RISC-V timer 在 Linux 中的实现][036], [直播回放][057] @yuliao

* swtimer @kyrie.xi
* udelay

## Synchronization

* Atomics
    * [RISC-V 原子指令与用法介绍][017], [直播回放][048] @pingbo
    * [RISC-V vs Aarch 64 atomic PK] @aabb5566
* [Generic Ticket Spinlock][010] @aabb5566
* [Mutex] https://lwn.net/Articles/164802/ @aabb5566
* [Restartable Sequence (RSEQ)][011]
* [Qspinlock][014]
* [A Memory Model for RISC-V] (https://riscv.org/wp-content/uploads/2018/05/14.25-15.00-RISCVMemoryModelTutorial.pdf) @aabb5566

## Multi-core

* SMP support @user_11186799
* IPI
* Hotplug
* CPUIdle
* Membarrier sync core @通天塔 

## Scheduling

* Multi-tasking @Jack Y.
* Context switch @Jack Y.
    * RISC-V Linux 内核调度详解，[直播回放][055]  @Jack Y.
    * [RISC-V Linux 上下文切换分析][018]  @Jack Y.
    * [RISC-V Linux __schedule() 分析][028]  @Jack Y.

* Task implementation @tjytimi
    * [RISC-V Linux 进程创建与执行流程代码分析][035]  @tjytimi
    * [RISC-V 架构下内核线程返回函数探究][024]  @tjytimi

* EAS

## User-space Support

* Syscall
    * RISC-V Linux 系统调用详解，[直播回放][056] @envestcc

* ELF support
* module implementation
* Multi-threads
* binfmt_flat

## Power Management

* Suspend To RAM
* Suspend To Disk
* Wakeup Source Support
* Clock Gating
* Regulator

## Thermal Management

## Tracing

* Stacktrace
    * [RISC-V Linux Stacktrace 详解][034], [直播回放][052] @zhao305149619

* Tracepoint
* Ftrace @sugarfillet
* [Fprobe][063]
* Kprobes @juliwang
* Uprobes
* [User Events][064]
* eBPF @jams_liu

## Debugging

* Ptrace
* Crash
* KGDB
* Kexec & Kdump

## Detection

* KCSAN
* KASAN
* KMSAN
* KFENCE
    * [Linux Kfence 详解][026]  @pwl999
* Kmemleak

## Profiling

* Perf
* PMU (Performance Monitor Unit)
* kcov

## Tuning

* vdso
* Jump Label
    * [RISC-V Linux jump_label 详解，第 1 部分：技术背景][020] @falcon
    * [RISC-V Linux jump_label 详解，第 2 部分：指令编码][021], [直播回放][050] @falcon
    * [RISC-V jump_label 详解，第 3 部分：核心实现][022] @falcon
    * [RISC-V jump_label 详解，第 4 部分：运行时代码改写][023] @falcon

* Static call

## Security

* Stackprotector
* OP-TEE for RISC-V
* PengLai for RISC-V

## Virtulization

* [KVM][013] @walimis
    * [直播回放：GPU Virtulization][059]  @walimis

## Fast boot

* XIP

## Real Time

* Preemption

## Benchmarking

* Microbench
    * [RISC-V 处理器指令级性能评测尝试][032]
    * [开源之夏 2022 - 处理器指令级 Microbench 开发实录][003]
* [martinus/nanobench][009]
* [andreas-abel/nanoBench][007]

## Testing

* Autotest frameworks

## Latest development

* [News][006]，已新建独立的 News 发布页

[001]: https://fmash16.github.io/content/posts/riscv-emulator-in-c.html
[002]: https://gitee.com/tinylab/cloud-lab/issues/I56CKU
[003]: https://gitee.com/tinylab/cloud-lab/issues/I57CM0
[004]: https://gitee.com/tinylab/riscv-linux/raw/master/ppt/riscv-isa.pptx
[005]: https://gitee.com/tinylab/riscv-linux/raw/master/ppt/riscv-rustsbi.pdf
[006]: https://gitee.com/tinylab/riscv-linux/tree/master/news
[007]: https://github.com/andreas-abel/nanoBench
[008]: https://github.com/juiceRv/JuiceVm
[009]: https://github.com/martinus/nanobench
[010]: https://lore.kernel.org/linux-riscv/11364105.8ZH9dyz9j6@diego/T/#t
[011]: https://lore.kernel.org/linux-riscv/20220308083253.12285-1-vincent.chen@sifive.com/T/#t
[012]: https://lore.kernel.org/linux-riscv/20220402101801.GA9428@amd/T/#t
[013]: https://lore.kernel.org/linux-riscv/CAAhSdy3JwLyOoNOubAS2VusNdj6O-CJJNSs+mM8+NvKErtAdmw@mail.gmail.com/T/#u
[014]: https://lore.kernel.org/linux-riscv/CAJF2gTT9YHgTzPaBN4ekYS7UcBOj_9k9xEcrsoXgW6PCZc8x3Q@mail.gmail.com/T/#t
[015]: https://tinylab.org/nezha-d1-archlinux/
[016]: https://tinylab.org/nezha-d1-quickstart/
[017]: https://tinylab.org/riscv-atomics/
[018]: https://tinylab.org/riscv-context-switch/
[019]: https://tinylab.org/riscv-isa-intro
[020]: https://tinylab.org/riscv-jump-label-part1/
[021]: https://tinylab.org/riscv-jump-label-part2/
[022]: https://tinylab.org/riscv-jump-label-part3/
[023]: https://tinylab.org/riscv-jump-label-part4/
[024]: https://tinylab.org/riscv-kthread-ret/
[025]: https://tinylab.org/riscv-linux-distros/
[026]: https://tinylab.org/riscv-linux-kfence/
[027]: https://tinylab.org/riscv-linux-quickstart/
[028]: https://tinylab.org/riscv-linux-schedule/
[029]: https://tinylab.org/riscv-linux-startup/
[030]: https://tinylab.org/riscv-mmu/
[031]: https://tinylab.org/riscv-opensbi-quickstart/
[032]: https://tinylab.org/riscv-perf-benchmark/
[033]: https://tinylab.org/riscv-privileged/
[034]: https://tinylab.org/riscv-stacktrace/
[035]: https://tinylab.org/riscv-task-implementation/
[036]: https://tinylab.org/riscv-timer/
[037]: https://tinylab.org/riscv-uefi-part1/
[038]: https://tinylab.org/run-riscv-ubuntu-over-x86/
[039]: https://tinylab.org/rv-link-debugger/
[040]: https://www.bilibili.com/video/BV1GV4y1H77p
[041]: https://www.bilibili.com/video/BV1L54y1o7Fs
[042]: https://www.cctalk.com/v/16477623213273
[043]: https://www.cctalk.com/v/16484079493695
[044]: https://www.cctalk.com/v/16484083661860
[045]: https://www.cctalk.com/v/16484095223067
[046]: https://www.cctalk.com/v/16484116944868
[047]: https://www.cctalk.com/v/16484116944869
[048]: https://www.cctalk.com/v/16489499392383
[049]: https://www.cctalk.com/v/16495466863205
[050]: https://www.cctalk.com/v/16501801841966
[051]: https://www.cctalk.com/v/16520639214118
[052]: https://www.cctalk.com/v/16525972863665
[053]: https://www.cctalk.com/v/16551875274459
[054]: https://www.cctalk.com/v/16551876033662
[055]: https://www.cctalk.com/v/16558003571816
[056]: https://www.cctalk.com/v/16562391483157
[057]: https://www.cctalk.net/v/16566997322462
[058]: https://www.cctalk.net/v/16574310111172
[059]: https://www.cctalk.net/v/16574325601701
[060]: https://www.cctalk.net/v/16586599461554
[061]: https://www.cctalk.net/v/16592486764588
[062]: https://www.cctalk.net/v/16598634312907
[063]: https://www.kernel.org/doc/html/latest/trace/fprobe.html
[064]: https://www.kernel.org/doc/html/latest/trace/user_events.html
